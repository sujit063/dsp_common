package com.dsp.common.dto.userrole;

import com.dsp.common.interfaces.IModal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRoleDTO implements IModal
{
	private static final long	serialVersionUID	= 1L;

	private int					id;
	private String				name;
	private int					roleType;
	private String				descriptaion;

}
