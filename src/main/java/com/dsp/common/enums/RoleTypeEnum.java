package com.dsp.common.enums;

public enum RoleTypeEnum
{
	ADMINSTRATOR(1, "Adminstrator"), VENDOR(2, "Vendor"), VENDOR_USER(3, "Vendor User"), END_USER(4, "End User");

	private int		roleType;
	private String	displayName;

	private RoleTypeEnum(int roleType, String displayName)
	{
		this.roleType = roleType;
		this.displayName = displayName;
	}

	public int getRoleType()
	{
		return roleType;
	}

	public String getDisplayName()
	{
		return displayName;
	}
}
